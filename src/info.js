const fs = require("fs");
const path = require("path");
const bcrypt = require("bcryptjs");
const inquirer = require("inquirer");
const dayjs = require("dayjs");
inquirer.registerPrompt("datepicker", require("inquirer-datepicker"));
// Contiene la ruta del archivo JSON
const rutaArchivo = path.join(__dirname, "data.json");
/**
 * Trae los datos que estan en el archivo JSON
 */
async function baseDatos() {
	//Abre el archivo
	if (fs.existsSync(rutaArchivo)) {
		//Lee la info del archivo
		let archivo = fs.readFileSync(rutaArchivo);
		let datos = JSON.parse(archivo);
		return datos;
	}
}

/**
 * Crea un nuevo usuario.
 * @param {array} datos donde se encuentran los datos
 * @param {object} nuevousuario a agregar
 */
const crearCuenta = async (datos, nuevousuario) => {
	// Crea el objeto de las categorias.
	nuevousuario.categorias = [{ nombre: "Sin categoria", gastos: [] }];
	nuevousuario.password = await bcrypt.hash(nuevousuario.password, 8);
	//Agrega nuevo usuario
	datos.usuarios.push(nuevousuario);

	//Convierte JSON a cadena de caracteres String
	let contenido = JSON.stringify(datos);

	// Escribe el contenido en el archivo
	fs.writeFileSync(rutaArchivo, contenido);

	console.log("Felicidades! Su cuenta ha sido creada.");
};

/**
 * Edita el nombre de la categoria seleccionada.
 * @param {Array} datoCategoria Tiene los datos de la categoria a editar.
 * @param {Object} nuevoNombre Es el nuevo nombre que quiere el usuario para la categoria.
 * @param {Array} datoUsuario Contiene los datos del usuario.
 */
async function editarCategoria(datoCategoria, nuevoNombre, datoUsuario) {
	// Trae la base de datos.
	let datos = await baseDatos();

	// Recorre la base de datos para poder modificar la categoria.
	for (const usuario of datos.usuarios) {
		if (datoUsuario.email === usuario.email) {
			let existecategoria = usuario.categorias.findIndex(
				b => b.nombre === nuevoNombre.nuevaCategoria
			);
			if (existecategoria === -1) {
				console.log(existecategoria);
				for (const categoria of usuario.categorias) {
					if (categoria.nombre === datoCategoria.nombre) {
						categoria.nombre = nuevoNombre.nuevaCategoria;
						console.log("SE EDITÓ LA CATEGORÍA CORRECTAMENTE");
					}
				}
			} else {
				console.log("EL NOMBRE DE LA CATEGORÍA YA ESTÁ EN USO, ELIJA OTRO");
			}
		}
	}
	// Convierte datos JSON a string.
	let contenido = JSON.stringify(datos);
	// Escribe el contenido en el archivo.
	fs.writeFileSync(rutaArchivo, contenido);
}

/**
 * Elima categoria, escoje si eliman gastos o los mueve a una categoría sin nombre
 * @param {Array} datoCategoria Información de la categoría que se escogió x el usuario
 * @param {Array} datoUsuario	Datos del usuario que inicia sesión
 * @param {Object} confirmacion Mira si elimina gastos y categorias, o eliminar la categoría y dejar los gastos
 */
async function eliminarCategoria(datoCategoria, datoUsuario, confirmacion) {
	// Trae la base de datos.
	let datos = await baseDatos();
	// Recorre la base de datos.
	for (const usuario of datos.usuarios) {
		if (usuario.email === datoUsuario.email) {
			// Busca la pocisión en que se encuenta la categoria escogida.
			let posicion = usuario.categorias.findIndex(
				b => b.nombre === datoCategoria.nombre
			);
			if (confirmacion === true) {
				//Elimina la categiria con los gastos
				usuario.categorias.splice(posicion, 1);
				console.log("ELIMINÓ CATEGORIA Y SUS GASTOS");
				// Convierte datos JSON a string.
				let contenido = JSON.stringify(datos);
				// Escribe el contenido en el archivo.
				fs.writeFileSync(rutaArchivo, contenido);
				break;
			} else {
				// Pasa los gastos de la categoria escogida a "Sin categoria"
				usuario.categorias[0].gastos = usuario.categorias[posicion].gastos;
				// Elimina
				usuario.categorias.splice(posicion, 1);
				// Convierte datos JSON a string.
				let contenido = JSON.stringify(datos);
				// Escribe el contenido en el archivo.
				fs.writeFileSync(rutaArchivo, contenido);
				console.log("ELIMINÓ CATEGORIA Y LOS GASTOS NO TIENEN CATEGORÍA");
				break;
			}
		}
	}
}

/**
 * Crea una nueva categoria y valida que no se repita con las otras.
 * @param {Array} usuarioregistrado Datos del usuario que inicia sesión.
 * @param {Object} nombrenuevacategoria Nombre de la categoria que se quiere crear.
 */
async function crearCategoria(usuarioregistrado, nombrenuevacategoria) {
	// Trae la base de datos.
	let datos = await baseDatos();
	// Busca Usuario.
	for (const usuario of datos.usuarios) {
		if (usuarioregistrado.email === usuario.email) {
			let posicion = usuario.categorias.findIndex(
				b => b.nombre === nombrenuevacategoria.nombre
			);
			if (posicion === -1) {
				nombrenuevacategoria.gastos = [];
				usuario.categorias.push(nombrenuevacategoria);
				console.log("La categoria se creo satisfactoriamente.");
			} else {
				console.log("No se pudo crear La categoria ya existe.");
			}
		}
	}
	// Convierte datos JSON a string.
	let contenido = JSON.stringify(datos);
	// Escribe el contenido en el archivo.
	fs.writeFileSync(rutaArchivo, contenido);
}

async function registrarGasto(
	usuarioregistrado,
	registrargasto,
	ubicacioncategoria
) {
	// Trae la base de datos.
	let datos = await baseDatos();
	registrargasto.monto = parseInt(registrargasto.monto);
	// Busca Usuario

	for (const usuario of datos.usuarios) {
		if (usuarioregistrado.email === usuario.email) {
			for (const categoriainput of usuario.categorias) {
				if (categoriainput.nombre === ubicacioncategoria.categorias) {
					categoriainput.gastos.push(registrargasto);
					console.log(categoriainput.gastos);
					break;
				}
			}
			break;
		}
	}
	// Convierte datos JSON a string.
	let contenido = JSON.stringify(datos);
	// Escribe el contenido en el archivo.
	fs.writeFileSync(rutaArchivo, contenido);
}

async function editarGasto(
	usuarioregistrado,
	categoriaSeleccionada,
	gastoEditado,
	posicionGasto
) {
	// Trae la base de datos.
	let datos = await baseDatos();
	// Busca Usuario
	for (const usuario of datos.usuarios) {
		if (usuarioregistrado.email === usuario.email) {
			console.log(usuario.categorias[categoriaSeleccionada].gastos);
			usuario.categorias[categoriaSeleccionada].gastos[posicionGasto] = gastoEditado;
			console.log(usuario.categorias[categoriaSeleccionada].gastos);
		}
	}
	// Convierte datos JSON a string.
	let contenido = JSON.stringify(datos);
	// Escribe el contenido en el archivo.
	fs.writeFileSync(rutaArchivo, contenido);
}

async function eliminarGasto(
	usuarioRegistrado,
	categoria,
	posicionGasto,
	confirm
) {
	// trae la base de datos JSON.
	let datos = await baseDatos();

	for (const usuario of datos.usuarios) {
		if (usuarioRegistrado.email === usuario.email) {
			if (confirm.confirmacion === true) {
				let gasto = usuario.categorias[categoria].gastos;
				gasto.splice(posicionGasto, 1);
			} else {
				break;
			}
		}
	}
	// Convierte datos JSON a string.
	let contenido = JSON.stringify(datos);
	// Escribe el contenido en el archivo.
	fs.writeFileSync(rutaArchivo, contenido);
}

async function ordenarFecha(gastos) {
	let arreglo = [];
	let arregloMostrar = [];
	// console.log(gastos);
	for (const gasto of gastos) {
		gasto.fecha = dayjs(gasto.fecha);
		gasto.fecha.format("YYYY/MM/DD");
		arreglo.push(gasto.fecha.format("YYYY/MM/DD"));
	}
	arreglo.sort();
	let contarr = 0;
	let contf = 0;

	while (true) {
		if (arregloMostrar.length === arreglo.length) {
			break;
		} else {
			if (arreglo[contarr] === gastos[contf].fecha.format("YYYY/MM/DD")) {
				arregloMostrar.push(gastos[contf]);
				arregloMostrar[contarr].fecha.format("YYYY/MM/DD");
				contarr++;
				contf = -1;
			}
			contf++;
		}
	}
	return arregloMostrar;
}

module.exports = {
	baseDatos,
	crearCuenta,
	editarCategoria,
	eliminarCategoria,
	crearCategoria,
	registrarGasto,
	editarGasto,
	eliminarGasto,
	ordenarFecha,
};
