const {
	baseDatos,
	crearCuenta,
	editarCategoria,
	eliminarCategoria,
	crearCategoria,
	registrarGasto,
	editarGasto,
	eliminarGasto,
	ordenarFecha,
} = require("./info");
const {
	menuInicial,
	credencialesNuevaCuenta,
	menuPerfil,
	credencialesInicioSesion,
	menuVerCategoria,
	menuOpcionesCategoria,
	credencialesCategoria,
	menuSinCategoria,
	menuConfirmacionGastos,
	credencialesCrearCategoria,
	credencialesRegistrarGasto,
	menuVerGastos,
	credencialesEditarGasto,
	menuVerListaGastos,
	confirmacionEliminarGasto,
	menuReporte,
	menuMeses,
} = require("./input");
const {
	mostrarReporteTotal,
	mostrarReporteCategoria,
	mostrarReporteMensual,
} = require("./output");
const dayjs = require("dayjs");

const inquirer = require("inquirer");
inquirer.registerPrompt("datepicker", require("inquirer-datepicker"));

const customParseFormat = require("dayjs/plugin/customParseFormat");
dayjs.extend(customParseFormat);

/**
 * Mueestra las opciones que puede realizar el usuario.
 * @param {Array} usuario Datos que contiene el usuario.
 */
async function menuInicioSesion(datoUsuario) {
	opcion = 0;
	while (opcion !== 6) {
		let seleccion = await menuPerfil();
		opcion = seleccion.seleccionPerfil;
		let informacion = await baseDatos();
		let indice = informacion.usuarios.findIndex(b => b.email === datoUsuario.email);
		let usuario = informacion.usuarios[indice];
		// for(const usa)
		switch (opcion) {
			case 1:
				// Ver categorias
				// Trae la categoria deseada por el usuario.
				console.log(usuario.categorias);
				// console.log(usuario.categorias.length);
				let datoIngresado = await menuVerCategoria(usuario.categorias);
				console.log(datoIngresado);

				// Busca la posicion que se encuentra esa categoria.
				let posicion = usuario.categorias.findIndex(
					b => b.nombre === datoIngresado.categorias
				);
				// Saca la información de la categoria.
				let categoria = usuario.categorias[posicion];

				await menuCategoria(usuario, categoria);
				break;
			case 2:
				// Registrar Gasto
				let registrargasto = await credencialesRegistrarGasto();
				let ubicacioncategoria = await menuVerCategoria(usuario.categorias);
				await registrarGasto(usuario, registrargasto, ubicacioncategoria);
				break;
			case 3:
				// Crear categoria
				console.log("Opción 3");
				let nombrenuevacategoria = await credencialesCrearCategoria();
				await crearCategoria(usuario, nombrenuevacategoria);
				break;
			case 4:
				// Ver Gastos
				let categoriaEscogida = await menuVerCategoria(usuario.categorias);
				let indice = usuario.categorias.findIndex(
					b => b.nombre === categoriaEscogida.categorias
				);
				let gastoEscogido = await menuVerGastos(usuario.categorias[indice].gastos);

				await menuModificarGastos(usuario, indice, gastoEscogido);
				break;
			case 5:
				// Consultar reporte
				console.log("Opción 5");
				await menuConsultarReporte(usuario);
				break;

			case 6:
				// Cerrar sesión
				break;
		}
	}
}

async function menuModificarGastos(usuario, categoriaEscogida, gastoEscogido) {
	let opcion = 0;
	while (opcion !== 3) {
		let seleccion = await menuVerListaGastos();
		opcion = seleccion.opcionGastos;
		switch (opcion) {
			case 1:
				// Editar Gasto

				let location = usuario.categorias[categoriaEscogida].gastos.findIndex(
					b => b.descripcion === gastoEscogido.listagastos
				);
				console.log(gastoEscogido);
				let nuevoGasto = await credencialesEditarGasto(
					usuario.categorias[categoriaEscogida].gastos[location]
				);

				await editarGasto(usuario, categoriaEscogida, nuevoGasto, location);

				await menuInicioSesion(usuario);

				break;
			case 2:
				// Eliminar Gasto
				let posicion = usuario.categorias[categoriaEscogida].gastos.findIndex(
					b => b.descripcion === gastoEscogido.listagastos
				);
				let confirmacion = await confirmacionEliminarGasto();
				await eliminarGasto(usuario, categoriaEscogida, posicion, confirmacion);
				await menuInicioSesion(usuario);

				break;
			case 3:
				// Volver
				break;
		}
	}
}

/**
 * Mueestra las opciones que se pueden realizar para modificar la categoria.
 * @param {Array} usuario Es el usuario autenticado.
 * @param {Array} categoria Contiene la información de la categoria.
 */
async function menuCategoria(usuario, categoria) {
	let opcion = 0;
	while (opcion !== 3) {
		let seleccion = await menuOpcionesCategoria();
		opcion = seleccion.opcionesCategoria;
		switch (opcion) {
			case 1:
				// Editar categoria.

				// Obtiene el nuevo nombre de la categoria a editar.
				let nuevoNombre = await credencialesCategoria();

				await editarCategoria(categoria, nuevoNombre, usuario);

				break;
			case 2:
				// Eliminar categoria.

				let confirmacion = await menuConfirmacionGastos();

				await eliminarCategoria(categoria, usuario, confirmacion.menuConfirmacionGastos);
				break;

			case 3:
				// Volver.
				break;
		}
	}
}

async function menuConsultarReporte(datoUsuario) {
	let opcion = 0;
	while (opcion !== 4) {
		let seleccion = await menuReporte();
		opcion = seleccion.datos;
		switch (opcion) {
			case 1:
				// Reporte por Categoria
				let data = await baseDatos();
				let categoria = await menuVerCategoria(datoUsuario.categorias);
				await mostrarReporteCategoria(data, datoUsuario, categoria);
				break;
			case 2:
				// Reporte mensual
				let informacion = await baseDatos();
				let mes = await menuMeses();
				await mostrarReporteMensual(informacion, datoUsuario, mes);

				break;
			case 3:
				// Reporte total
				let datos = await baseDatos();
				await mostrarReporteTotal(datos, datoUsuario, 9);

				break;
			case 4:
				// Volver
				break;
		}
	}
}

module.exports = {
	menuInicial,
	baseDatos,
	credencialesNuevaCuenta,
	crearCuenta,
	menuPerfil,
	menuInicioSesion,
	credencialesInicioSesion,
	menuCategoria,
};
