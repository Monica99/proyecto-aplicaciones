const dayjs = require("dayjs");
const inquirer = require("inquirer");
const { baseDatos } = require("./info");

inquirer.registerPrompt("datepicker", require("inquirer-datepicker"));

/**
 * Genera el menu Inicial para iniciar sesión o crear cuenta.
 */
async function menuInicial() {
	const opciones = [
		{
			name: "menu",
			type: "rawlist",
			message: "Inicie sesión o cree una cuenta",
			choices: [
				{ value: 1, name: "Crear Usuario" },
				{ value: 2, name: "Iniciar sesión" },
				{ value: 3, name: "Salir" },
			],
		},
	];
	return inquirer.prompt(opciones);
}

/**
 * Pide los datos del usuario para crear una cuenta.
 */
async function credencialesNuevaCuenta() {
	const opciones = [
		{
			name: "firstName",
			type: "input",
			message: "Nombre:",
		},
		{
			name: "lastName",
			type: "input",
			message: "Apellido:",
		},
		{
			name: "email",
			type: "input",
			message: "Email:",
			// REALIZA LA VALIDACIÓN DEL CORREO
			validate: async function (input) {
				let listaUsuarios = await baseDatos();

				// REVISA QUE SEA UN CORREO CON @ Y CARACTERES ESPECIALES
				function validateEmail(email) {
					const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
					return re.test(String(email).toLowerCase());
				}
				// BUSCA SI SE REPITE EL EMAIL EN LA BhxBrowser.DD DE JSON
				const validacion = listaUsuarios.usuarios.findIndex(b => b.email === input);
				let formato = validateEmail(input);
				// IF QUE VALIDA QUE NO EXISTA EL CORREO NI SEA INVÁLIDO
				return formato === true && validacion === -1
					? true
					: "El email ya existe o verifique el email digitado";
			},
		},
		{
			name: "password",
			type: "password",
			message: "Contraseña:",
			mask: true,
		},
	];
	return inquirer.prompt(opciones);
}

/**
 * Genera las opciones que puede hacer el perfil.
 */
async function menuPerfil() {
	const opciones = [
		{
			name: "seleccionPerfil",
			type: "rawlist",
			message: "Bienvenido",
			choices: [
				{ value: 1, name: "Ver categorias" },
				{ value: 2, name: "Registrar gasto" },
				{ value: 3, name: "Crear categoria" },
				{ value: 4, name: "Ver Gastos" },
				{ value: 5, name: "Consultar reportes" },
				{ value: 6, name: "Cerrar sesión" },
			],
		},
	];
	return inquirer.prompt(opciones);
}

async function menuVerListaGastos() {
	const opciones = [
		{
			name: "opcionGastos",
			type: "rawlist",
			choices: [
				{ value: 1, name: "Editar Gastos" },
				{ value: 2, name: "Eliminar Gasto" },
				{ value: 3, name: "Volver" },
			],
		},
	];
	return inquirer.prompt(opciones);
}

/**
 * Pide los datos del usuario para iniciar sesión.
 */
async function credencialesInicioSesion() {
	const datos = [
		{
			name: "correo",
			type: "input",
			message: "Ingrese correo electrónico:",
		},
		{
			name: "password",
			type: "password",
			message: "Ingrese la contraseña:",
			mask: true,
		},
	];
	return inquirer.prompt(datos);
}

/**
 *  Muesta las categorias del usuario en forma de lista.
 * @param {Array} datos datos del usuario.
 */
async function menuVerCategoria(datos) {
	let arreglo = [];
	for (const dato of datos) {
		// Se agregan las categorias a un arreglo vacio.
		arreglo.push({ name: dato.nombre, vaule: dato.nombre });
	}
	const opciones = [
		{
			name: "categorias",
			type: "rawlist",
			message: "Seleccione la categoria",
			choices: arreglo,
		},
	];
	return inquirer.prompt(opciones);
}

/**
 * Genera las opciones que se pueden realizar en una categoria.
 */
async function menuOpcionesCategoria() {
	const opciones = [
		{
			name: "opcionesCategoria",
			type: "rawlist",
			message: "Ingrese la opción necesaria:",
			choices: [
				{ value: 1, name: "Editar categoria" },
				{ value: 2, name: "Eliminar categoria" },
				{ value: 3, name: "Volver" },
			],
		},
	];
	return inquirer.prompt(opciones);
}

/**
 * Pide la el nombre de una categoria.
 */
async function credencialesCategoria() {
	const opciones = [
		{
			name: "nuevaCategoria",
			type: "input",
			message: "Ingrese el nombre de la categoria:",
		},
	];
	return inquirer.prompt(opciones);
}

/**
 * Genera las opciones que debe tener la opción 'Sin Categoria'.
 */
async function menuSinCategoria() {
	const opciones = [
		{
			name: "menuSinCategoria",
			type: "rawlist",
			message: "Escoja la opción deseada",
			choices: [
				{ value: 1, name: "Registrar gasto" },
				{ value: 2, name: "Editar gasto" },
				{ value: 3, name: "Eliminar gasto" },
				{ value: 4, name: "Volver" },
			],
		},
	];
	return inquirer.prompt(opciones);
}

/**
 * Genera dos opciones para confirmar la eliminación de una categoria.
 */
async function menuConfirmacionGastos() {
	const opciones = [
		{
			name: "menuConfirmacionGastos",
			type: "confirm",
			message: "¿Desea eliminar los gastos que se encuentran en la categoría?",
		},
	];
	return inquirer.prompt(opciones);
}

async function credencialesCrearCategoria() {
	const opciones = [
		{
			name: "nombre",
			type: "input",
			message: "Digite el nombre de la NUEVA categoría",
		},
	];
	return inquirer.prompt(opciones);
}

async function credencialesRegistrarGasto() {
	const opciones = [
		{
			name: "fecha",
			type: "datepicker",
			message: "Digite la fecha en que se realizo el gasto:",
			format: ["Y", "/", "MM", "/", "DD"],
			validate: async function (input) {
				return input !== "" ? true : "Este campo es obligatorio";
			},
		},
		{
			name: "descripcion",
			type: "input",
			message: "Digite la descripcion del gasto:",
			validate: async function (input) {
				return input !== "" ? true : "Este campo es obligatorio";
			},
		},
		{
			name: "monto",
			type: "input",
			message: "Ingrese el monto del gasto:",
			validate: async function (input) {
				// return input !== "" ? true : "Este campo es obligatorio";
				input = parseInt(input, 10);
				if (input <= 0 || isNaN(input) || input === "") {
					return "Digite un valor válido";
				} else {
					return true;
				}
			},
		},
	];
	return inquirer.prompt(opciones);
}

async function menuVerGastos(datos) {
	let arreglo = [];
	for (const dato of datos) {
		let mostrar = dayjs(dato.fecha);
		// Se agregan las categorias a un arreglo vacio.
		arreglo.push({
			value: dato.descripcion,
			name: `Fecha: ${mostrar.format("ddd, DD/MMM/YYYY")} Descripción: ${
				dato.descripcion
			} Monto: ${dato.monto}`,
		});
	}
	const opciones = [
		{
			name: "listagastos",
			type: "rawlist",
			message: "Elija el gasto que desea editar",
			choices: arreglo,
		},
	];
	return inquirer.prompt(opciones);
}

async function credencialesEditarGasto(datos) {
	const opciones = [
		{
			name: "fecha",
			type: "datepicker",
			message: "Modifique la fecha en que se realizo el gasto:",
			format: ["Y", "/", "MM", "/", "DD"],
			default: datos.fecha,
		},
		{
			name: "descripcion",
			type: "input",
			message: "Digite la descripcion del gasto:",
			default: datos.descripcion,
		},
		{
			name: "monto",
			type: "input",
			message: "Ingrese el monto del gasto:",
			validate: async function (input) {
				// return input !== "" ? true : "Este campo es obligatorio"
				input = parseInt(input, 10);
				if (input <= 0 || isNaN(input) || input === "") {
					return "Digite un valor válido";
				} else {
					return true;
				}
			},
			default: datos.monto,
		},
	];
	return inquirer.prompt(opciones);
}

async function confirmacionEliminarGasto() {
	const opciones = [
		{
			name: "confirmacion",
			type: "confirm",
			message: "¿Está segiro que quiere eliminar el gasto?",
		},
	];
	return inquirer.prompt(opciones);
}

async function menuReporte() {
	const opciones = [
		{
			name: "datos",
			type: "rawlist",
			message: "Seleccione el reporte que desea consultar",
			choices: [
				{ value: 1, name: "Reporte por categoria" },
				{ value: 2, name: "Reporte mensual" },
				{ value: 3, name: "Reporte total" },
				{ value: 4, name: "Volver" },
			],
		},
	];
	return inquirer.prompt(opciones);
}

async function menuMeses() {
	const opciones = [
		{
			name: "meses",
			type: "rawlist",
			message: "Seleccione el mes que quiere ver gastos:",
			choices: [
				{ value: 1, name: "Enero" },
				{ value: 2, name: "Febrero" },
				{ value: 3, name: "Marzo" },
				{ value: 4, name: "Abril" },
				{ value: 5, name: "Mayo" },
				{ value: 6, name: "Junio" },
				{ value: 7, name: "Julio" },
				{ value: 8, name: "Agosto" },
				{ value: 9, name: "Septiembre" },
				{ value: 10, name: "Octubre" },
				{ value: 11, name: "Noviembre" },
				{ value: 12, name: "Diciembre" },
			],
		},
	];
	return inquirer.prompt(opciones);
}

module.exports = {
	menuInicial,
	credencialesNuevaCuenta,
	menuPerfil,
	credencialesInicioSesion,
	menuVerCategoria,
	menuOpcionesCategoria,
	credencialesCategoria,
	menuSinCategoria,
	menuConfirmacionGastos,
	credencialesCrearCategoria,
	credencialesRegistrarGasto,
	menuVerGastos,
	credencialesEditarGasto,
	menuVerListaGastos,
	confirmacionEliminarGasto,
	menuReporte,
	menuMeses,
};
