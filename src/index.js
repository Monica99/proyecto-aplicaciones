const bcrypt = require("bcryptjs");
const {
	menuInicial,
	credencialesNuevaCuenta,
	baseDatos,
	crearCuenta,
	menuInicioSesion,
	credencialesInicioSesion,
} = require("./menus");

async function main() {
	let opcion = 0;
	while (opcion !== 3) {
		let dato = await menuInicial();
		opcion = dato.menu;
		let informacion = await baseDatos();
		switch (opcion) {
			case 1:
				// Recibe los datos del usuario.
				let datosUsuario = await credencialesNuevaCuenta();
				await crearCuenta(informacion, datosUsuario);
				break;
			case 2:
				let datosIngresados = await credencialesInicioSesion();
				let prueba = {};
				async function checkUser(username, password, hash) {
					const match = await bcrypt.compare(password, hash);
					if (match) {
						await menuInicioSesion(username);
					}
				}
				for (const usuario of informacion.usuarios) {
					if (usuario.email === datosIngresados.correo) {
						await checkUser(usuario, datosIngresados.password, usuario.password);
					}
				}
				break;
			case 3:
				break;
		}
	}
}

main();
