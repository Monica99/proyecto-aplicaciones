const dayjs = require("dayjs");
const { ordenarFecha } = require("./info");

async function mostrarReporteCategoria(datos, datoUsuario, datoCategoria) {
	for (const usuario of datos.usuarios) {
		if (usuario.email === datoUsuario.email) {
			for (const categoria of usuario.categorias) {
				if (categoria.nombre === datoCategoria.categorias) {
					console.log(categoria.nombre + ":\n");
					let orden = await ordenarFecha(categoria.gastos);

					for (const gasto of orden) {
						console.log(
							`Fecha: ${gasto.fecha.format("ddd, DD/MMM/YYYY")} Descripción: ${
								gasto.descripcion
							} Monto: ${gasto.monto}`
						);
					}
					console.log("\n");
				}
			}
		}
	}
}

async function mostrarReporteMensual(datos, datoUsuario, datoMes) {
	let arreglo = [];
	for (const usuario of datos.usuarios) {
		if (usuario.email === datoUsuario.email) {
			for (const categoria of usuario.categorias) {
				for (const gasto of categoria.gastos) {
					let conversionFecha = dayjs(gasto.fecha);
					let mes = conversionFecha.month() + 1;
					// console.log(mes, datoMes);
					if (mes === datoMes.meses) {
						arreglo.push(gasto);
					}
				}
			}
		}
	}
	console.log(arreglo);
	let ordenar = await ordenarFecha(arreglo);
	for (const mostrar of ordenar) {
		console.log(
			`Fecha: ${mostrar.fecha.format("ddd, DD/MMM/YYYY")} Descripción: ${
				mostrar.descripcion
			} Monto: ${mostrar.monto}`
		);
	}
}

async function mostrarReporteTotal(datos, datoUsuario) {
	for (const usuario of datos.usuarios) {
		if (usuario.email === datoUsuario.email) {
			for (const categoria of usuario.categorias) {
				console.log(categoria.nombre + ":");
				let ordenar = await ordenarFecha(categoria.gastos);
				for (const gasto of ordenar) {
					console.log(
						`Fecha: ${gasto.fecha.format("ddd, DD/MMM/YYYY")} Descripción: ${
							gasto.descripcion
						} Monto: ${gasto.monto}`
					);
				}
				console.log("\n");
			}
		}
	}
}

module.exports = {
	mostrarReporteTotal,
	mostrarReporteCategoria,
	mostrarReporteMensual,
};
